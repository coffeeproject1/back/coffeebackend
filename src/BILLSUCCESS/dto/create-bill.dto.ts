export class CreateBillDto {
  invoice: string;

  // date: string

  namepd: string;

  amountbill: number;

  units: string;

  priceperunits: number;

  discount: number;

  price: number;

  time: string;

  timepay: string;

  status: string;

  isChecked?: boolean;
}
