import { Injectable } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
  ) {}

  create(createBillDto: CreateBillDto): Promise<Bill> {
    return this.billsRepository.save(createBillDto);
  }

  findAll(): Promise<Bill[]> {
    return this.billsRepository.find();
  }

  findOne(id: number) {
    return this.billsRepository.findOneBy({ id });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    await this.billsRepository.update(id, updateBillDto);
    const bill = await this.billsRepository.findOneBy({ id });
    return bill;
  }

  async remove(id: number) {
    const deleteBill = await this.billsRepository.findOneBy({ id });
    return this.billsRepository.remove(deleteBill);
  }
}
