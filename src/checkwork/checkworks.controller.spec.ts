import { Test, TestingModule } from '@nestjs/testing';
import { CheckworksController } from './checkworks.controller';
import { CheckworksService } from './checkworks.service';

describe('CheckworksController', () => {
  let controller: CheckworksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckworksController],
      providers: [CheckworksService],
    }).compile();

    controller = module.get<CheckworksController>(CheckworksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
