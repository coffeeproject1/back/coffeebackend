import { Injectable } from '@nestjs/common';
import { CreateCheckworkDto } from './dto/create-checkwork.dto';
import { UpdateCheckworkDto } from './dto/update-checkwork.dto';
import { Repository } from 'typeorm';
import { Checkwork } from './entities/checkwork.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CheckworksService {
  constructor(
    @InjectRepository(Checkwork)
    private checkworksRepository: Repository<Checkwork>,
  ) {}
  create(createCheckworkDto: CreateCheckworkDto) {
    const checkwork = new Checkwork();
    checkwork.fullname = createCheckworkDto.fullname;
    checkwork.password = createCheckworkDto.password;
    checkwork.date = createCheckworkDto.date;
    checkwork.timein = createCheckworkDto.timein;
    checkwork.timeout = createCheckworkDto.timeout;
    checkwork.status = createCheckworkDto.status;
    return this.checkworksRepository.save(checkwork);
  }

  findAll() {
    return this.checkworksRepository.find({});
  }

  findOne(id: number) {
    return this.checkworksRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateCheckworkDto: UpdateCheckworkDto) {
    const checkworkToUpdate = await this.checkworksRepository.findOneOrFail({
      where: { id },
    });
    checkworkToUpdate.fullname = updateCheckworkDto.fullname;
    checkworkToUpdate.password = updateCheckworkDto.password;
    checkworkToUpdate.date = updateCheckworkDto.date;
    checkworkToUpdate.timein = updateCheckworkDto.timein;
    checkworkToUpdate.timeout = updateCheckworkDto.timeout;
    checkworkToUpdate.status = updateCheckworkDto.status;
    return this.checkworksRepository.save(checkworkToUpdate);
  }

  async remove(id: number) {
    const deleteProduct = await this.checkworksRepository.findOneOrFail({
      where: { id },
    });
    await this.checkworksRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
