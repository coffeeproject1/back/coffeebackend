import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Checkwork {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullname: string;

  @Column()
  password: string;

  @Column()
  date: string;

  @Column()
  timein: string;

  @Column()
  timeout: string;

  @Column()
  status: string;
}
