import { Module } from '@nestjs/common';
import { CheckworksService } from './checkworks.service';
import { CheckworksController } from './checkworks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkwork } from './entities/checkwork.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Checkwork])],
  controllers: [CheckworksController],
  providers: [CheckworksService],
  exports: [CheckworksService],
})
export class CheckworksModule {}
