import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckworkDto } from './create-checkwork.dto';

export class UpdateCheckworkDto extends PartialType(CreateCheckworkDto) {
  [x: string]: any;
}
