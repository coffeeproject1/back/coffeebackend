export class CreateCheckworkDto {
  fullname: string;
  password: string;
  date: string;
  timein: string;
  timeout: string;
  status: string;
}
