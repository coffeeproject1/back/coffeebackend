export class CreateIngredientDto {
  name: string;
  price: string;
  amount: number;
  image: string;
}
