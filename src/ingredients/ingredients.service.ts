import { Injectable } from '@nestjs/common';
import { CreateIngredientDto } from './dto/create-ingredient.dto';
import { UpdateIngredientDto } from './dto/update-ingredient.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Ingredient } from './entities/ingredient.entity';
import { Repository } from 'typeorm';

@Injectable()
export class IngredientsService {
  constructor(
    @InjectRepository(Ingredient)
    private ingredientsRepository: Repository<Ingredient>,
  ) {}
  create(createIngredientDto: CreateIngredientDto) {
    const ingredient = new Ingredient();
    ingredient.name = createIngredientDto.name;
    ingredient.price = parseFloat(createIngredientDto.price);
    ingredient.amount = createIngredientDto.amount;
    if (createIngredientDto.image && createIngredientDto.image !== '') {
      ingredient.image = createIngredientDto.image;
    }
    return this.ingredientsRepository.save(ingredient);
  }

  findAll() {
    return this.ingredientsRepository.find();
  }

  findOne(id: number) {
    return this.ingredientsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateIngredientDto: UpdateIngredientDto) {
    const ingredient = await this.ingredientsRepository.findOneOrFail({
      where: { id },
    });
    ingredient.name = updateIngredientDto.name;
    ingredient.price = parseFloat(updateIngredientDto.price);
    ingredient.amount = updateIngredientDto.amount;
    if (updateIngredientDto.image && updateIngredientDto.image !== '') {
      ingredient.image = updateIngredientDto.image;
    }
    this.ingredientsRepository.save(ingredient);
    const result = await this.ingredientsRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteIngredient = await this.ingredientsRepository.findOneOrFail({
      where: { id },
    });
    await this.ingredientsRepository.remove(deleteIngredient);

    return deleteIngredient;
  }
}
