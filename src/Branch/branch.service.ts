import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Repository } from 'typeorm';
import { Branch } from './entities/branch.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BranchsService {
  constructor(
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
  ) {}
  create(createBranchDto: CreateBranchDto) {
    const branch = new Branch();
    branch.name = createBranchDto.name;
    branch.tel = createBranchDto.tel;
    return this.branchsRepository.save(branch);
  }

  findAll() {
    return this.branchsRepository.find({});
  }

  findOne(id: number) {
    return this.branchsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    const branch = new Branch();
    branch.name = updateBranchDto.name;
    branch.tel = updateBranchDto.tel;
    const updateBranch = await this.branchsRepository.findOneOrFail({
      where: { id },
    });
    updateBranch.name = branch.name;
    updateBranch.tel = branch.tel;
    return this.branchsRepository.save(updateBranch);
  }

  async remove(id: number) {
    const deleteProduct = await this.branchsRepository.findOneOrFail({
      where: { id },
    });
    await this.branchsRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
