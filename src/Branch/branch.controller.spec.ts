import { Test, TestingModule } from '@nestjs/testing';
import { BranchsController } from './branch.controller';
import { BranchsService } from './branch.service';

describe('BranchsController', () => {
  let controller: BranchsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchsController],
      providers: [BranchsService],
    }).compile();

    controller = module.get<BranchsController>(BranchsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
