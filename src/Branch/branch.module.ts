import { Module } from '@nestjs/common';
import { BranchsService } from './branch.service';
import { BranchsController } from './branch.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Branch])],
  controllers: [BranchsController],
  providers: [BranchsService],
  exports: [BranchsService],
})
export class BranchModule {}
