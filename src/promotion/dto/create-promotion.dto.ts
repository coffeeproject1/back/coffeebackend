export class CreatePromotionDto {
  id?: number;
  name: string;
  discount: number;
}
