import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Repository } from 'typeorm';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionsRepository: Repository<Promotion>,
  ) {}
  create(createPromotionDto: CreatePromotionDto) {
    const promotion = new Promotion();
    promotion.name = createPromotionDto.name;
    promotion.discount = createPromotionDto.discount;
    // ingredient.price = parseFloat(createIngredientDto.price);
    return this.promotionsRepository.save(promotion);
  }

  findAll() {
    return this.promotionsRepository.find({});
  }

  findOne(id: number) {
    return this.promotionsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    const promotionToUpdate = await this.promotionsRepository.findOneOrFail({
      where: { id },
    });
    promotionToUpdate.name = updatePromotionDto.name;
    promotionToUpdate.discount = updatePromotionDto.discount;
    return this.promotionsRepository.save(promotionToUpdate);
  }

  async remove(id: number) {
    const deleteProduct = await this.promotionsRepository.findOneOrFail({
      where: { id },
    });
    await this.promotionsRepository.remove(deleteProduct);
    return deleteProduct;
  }
}
