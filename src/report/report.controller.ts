import { Controller, Get } from '@nestjs/common';
import { ReportService } from './report.service';

@Controller('report')
export class ReportController {
    constructor(private reportService: ReportService) { }
    @Get('/report1')
    report1() {
        return this.reportService.report1();
    }
    @Get('/report2')
    report2() {
        return this.reportService.report2();
    }
    @Get('/reportDaily')
    report3() {
        return this.reportService.reportexpensesDay();
    }
    @Get('/reportMount')
    reportexpensesMount() {
        return this.reportService.reportexpensesMount();
    }
    @Get('/reportYear')
    reportexpensesYear() {
        return this.reportService.reportexpensesYear();
    }
}

