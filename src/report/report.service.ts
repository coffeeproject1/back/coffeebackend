import { Injectable } from '@nestjs/common';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportService {
    constructor(private dataSource: DataSource) { }
    report1() {
        return this.dataSource.query(`SELECT a.ArtistId, a.Title, t.Name, SUM(UnitPrice) as Price 
        FROM invoice_items it LEFT JOIN tracks t ON  it.TrackId = t.TrackId
      LEFT JOIN albums a ON a.AlbumId = t.AlbumId
      GROUP BY t.AlbumId
      ORDER BY Price DESC`);
    }
    report2() {
        return this.dataSource.query(`SELECT SUM(o.total) AS INCOME, SUM(b.price) AS EXPENSES
            FROM order_item o, bill b;`);
    }
    reportexpensesDay() {
        return this.dataSource.query(`SELECT SUBSTR(time, 4, 2) || '/' || SUBSTR(time, 1, 2) || '/' || SUBSTR(time, 7, 4) AS formatted_time, SUM(PRICE) AS EXPENSES
        FROM BILL
        GROUP BY formatted_time;`);
    }
    reportexpensesMount() {
        return this.dataSource.query(`SELECT 
        CAST(SUBSTR(time, 1, 2) AS INTEGER) AS Month,
        SUM(PRICE) AS TotalPrice
      FROM BILL
      GROUP BY CAST(SUBSTR(time, 1, 2) AS INTEGER)
      ORDER BY Month;`);
    }
    reportexpensesYear() {
        return this.dataSource.query(`SELECT 
            CAST(SUBSTR(time, 7, 4) AS INTEGER) AS Year,
            SUM(PRICE) AS TotalPrice
          FROM BILL
          GROUP BY CAST(SUBSTR(time, 7, 4) AS INTEGER)
          ORDER BY Year;`
        );
    }
}
