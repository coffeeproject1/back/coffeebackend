import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
import { OrdersModule } from './orders/orders.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Member } from './member/entities/member.entity';
import { MembersModule } from './member/members.module';
import { Checkwork } from './checkwork/entities/checkwork.entity';
import { CheckworksModule } from './checkwork/checkworks.module';
import { IngredientsModule } from './ingredients/ingredients.module';
import { Ingredient } from './ingredients/entities/ingredient.entity';
import { Bill } from './BILLSUCCESS/entities/bill.entity';
import { BillModule } from './BILLSUCCESS/bills.module';
import { Branch } from './branch/entities/branch.entity';
import { BranchModule } from './branch/branch.module';
import { Promotion } from './promotion/entities/promotion.entity';
import { PromotionsModule } from './promotion/promotion.module';
import { ReportModule } from './report/report.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        User,
        Role,
        Type,
        Product,
        Order,
        OrderItem,
        Member,
        Ingredient,
        Order,
        OrderItem,
        Member,
        Checkwork,
        Bill,
        Branch,
        Promotion,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    RolesModule,
    TypesModule,
    ProductsModule,
    UsersModule,
    OrdersModule,
    AuthModule,
    MembersModule,
    IngredientsModule,
    CheckworksModule,
    BillModule,
    BranchModule,
    PromotionsModule,
    ReportModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
